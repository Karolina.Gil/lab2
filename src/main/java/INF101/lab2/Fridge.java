package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge implements IFridge{

	private final int max_size = 20;
	ArrayList<FridgeItem> itemsInFridge = new ArrayList<FridgeItem>(); 
	List<FridgeItem> expiredItems = new ArrayList<FridgeItem>(); 

	
	@Override
	public int nItemsInFridge() {
		return itemsInFridge.size();
	}

	
	@Override
	public int totalSize() {
		return max_size ;
	}
	
	
	@Override
	public boolean placeIn(FridgeItem item){
		if (itemsInFridge.size()<max_size) {
			itemsInFridge.add(item);
			return true;
		}
		else {
			return false;
			}
	}
	
	@Override
	public void takeOut(FridgeItem item) {
		//remove item from 
		if (!itemsInFridge.contains(item)) {
			throw new NoSuchElementException();
		}
		else {
			itemsInFridge.remove(itemsInFridge.indexOf(item));
		}
	}
	
	@Override
	public void emptyFridge() {
		itemsInFridge.clear();
		
	}
	
	@Override
	public List<FridgeItem> removeExpiredFood(){
		int s = itemsInFridge.size();
		for(int i = 0; i<s-1; i ++) {
			if (itemsInFridge.get(i).hasExpired()) {
				expiredItems.add(itemsInFridge.get(i));
			}
		
		}
		for(FridgeItem item : expiredItems) {
			itemsInFridge.remove(item);
		}
		
		return expiredItems;
		
	}
}
